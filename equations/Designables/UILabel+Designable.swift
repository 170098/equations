//
//  UIButton+Designable.swift
//  equations
//
//  Created by Alex de Waal on 2019/09/19.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class DesignableLabel : UILabel {
    
    @IBInspectable public var spacing: CGFloat = 0.0 {
        didSet {
            applyKerning()
        }
    }
    
    override var text: String? {
        didSet {
            applyKerning()
        }
    }
    
    private func applyKerning() {
        let stringValue = self.text ?? ""
        let attrString = NSMutableAttributedString(string: stringValue)
        attrString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
    
}

//
//  SPPermissions+Extensions.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/16.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import SPPermission

extension IntroViewController: SPPermissionDialogDataSource {

    var dialogTitle: String { return "Permissions Needed" }
    var dialogSubtitle: String { return "Equations" }
    var dialogComment: String { return "" }
    var allowTitle: String { return "Grant" }
    var allowedTitle: String { return "Granted" }
    var bottomComment: String { return "" }

    func name(for permission: SPPermissionType) -> String? { return nil }
    func description(for permission: SPPermissionType) -> String? { return nil }
    func deniedTitle(for permission: SPPermissionType) -> String? { return nil }
    func deniedSubtitle(for permission: SPPermissionType) -> String? { return nil }

    var cancelTitle: String { return "Cancel" }
    var settingsTitle: String { return "Settings" }
    
    var showCloseButton: Bool {
        return true
    }
}

//
//  ViewController+Extension.swift
//  equations
//
//  Created by Alex de Waal on 2019/09/30.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit

extension DashboardViewController {
    override func viewWillAppear(_ animated: Bool) {
        adjustConstraints()
    }
    
    func adjustConstraints() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
            case 1334:
                print("iPhone 6/6S/7/8")
                headerImageView.isHidden = true
                headerConstraint.constant = 200
                
                additionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 135, bottom: 0, right: 0)
                subtractionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 140, bottom: 0, right: 0)
                divisionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 135, bottom: 0, right: 0)
                multiplicationButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 135, bottom: 0, right: 0)
                
                multiplicationButton.titleEdgeInsets = UIEdgeInsets(top: 50, left: -9, bottom: 0, right: 0)
            case 1792:
                print("iPhone 11/XR")
                headerConstraint.constant = 350
                
                additionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 155, bottom: 0, right: 0)
                subtractionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 160, bottom: 0, right: 0)
                divisionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 155, bottom: 0, right: 0)
                multiplicationButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 155, bottom: 0, right: 0)
            case 2208:
                print("iPhone 6+/6S+/7+/8+")
                headerConstraint.constant = 250
                headerImageTopConstraint.constant = 50
                headerImageHeightConstraint.constant = 100
                headerImageWidthConstraint.constant = 100
                
                additionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 155, bottom: 0, right: 0)
                subtractionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 160, bottom: 0, right: 0)
                divisionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 155, bottom: 0, right: 0)
                multiplicationButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 155, bottom: 0, right: 0)
            case 2436:
                print("iPhone X/11 Pro")
                headerConstraint.constant = 300
                headerImageTopConstraint.constant = 70
                headerImageHeightConstraint.constant = 120
                headerImageWidthConstraint.constant = 120
                
                additionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 135, bottom: 0, right: 0)
                subtractionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 155, bottom: 0, right: 0)
                divisionButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 135, bottom: 0, right: 0)
                multiplicationButton.imageEdgeInsets = UIEdgeInsets(top: -45, left: 135, bottom: 0, right: 0)
            default:
                print("unknown")
            }
        }
    }
}

extension LeaderboardViewController {
    override func viewWillAppear(_ animated: Bool) {
        adjustConstraints()
    }
    
    func adjustConstraints() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
            case 1334:
                print("iPhone 6/6S/7/8")
                headerImageView.isHidden = true
                headerConstraint.constant = 200
            case 1792:
                print("iPhone 11/XR")
                headerConstraint.constant = 350
            case 2208:
                print("iPhone 6+/6S+/7+/8+")
                headerConstraint.constant = 250
                headerImageTopConstraint.constant = 50
                headerImageHeightConstraint.constant = 100
                headerImageWidthConstraint.constant = 140
            case 2436:
                print("iPhone X/11 Pro")
                headerConstraint.constant = 300
                headerImageTopConstraint.constant = 70
                headerImageHeightConstraint.constant = 120
                headerImageWidthConstraint.constant = 160
            default:
                print("unknown")
            }
        }
    }
}


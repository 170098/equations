//
//  FeedbackPageBLTNItem.swift
//  equations
//
//  Created by Alex de Waal on 2019/09/23.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import BLTNBoard

class FeedbackPageBLTNItem: BLTNPageItem {
    private let feedbackGenerator = SelectionFeedbackGenerator()
    
    override func actionButtonTapped(sender: UIButton) {
        
        playHapticFeedback() 
        super.actionButtonTapped(sender: sender)
        
    }
    
    fileprivate func playHapticFeedback() {
        feedbackGenerator.prepare()
        feedbackGenerator.selectionChanged()
    }
    
    override func alternativeButtonTapped(sender: UIButton) {
        
        playHapticFeedback()
        super.alternativeButtonTapped(sender: sender)
        
    }
}

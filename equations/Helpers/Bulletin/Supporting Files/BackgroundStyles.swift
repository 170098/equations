//
//  BackgroundStyles.swift
//  equations
//
//  Created by Alex de Waal on 2019/09/23.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import BLTNBoard

func BackgroundStyles() -> [(name: String, style: BLTNBackgroundViewStyle)] {
    
    var styles: [(name: String, style: BLTNBackgroundViewStyle)] = [
        ("None", .none),
        ("Dimmed", .dimmed)
    ]
    
    if #available(iOS 10, *) {
        styles.append(("Extra Light", .blurredExtraLight))
        styles.append(("Light", .blurredLight))
        styles.append(("Dark", .blurredDark))
        styles.append(("Extra Dark", .blurred(style: UIBlurEffect.Style(rawValue: 3)!, isDark: true)))
    }
    
    return styles
    
}

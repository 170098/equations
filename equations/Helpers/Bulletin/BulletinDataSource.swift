//
//  BulletinDataSource.swift
//  equations
//
//  Created by Alex de Waal on 2019/09/23.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import BLTNBoard

enum BulletinDataSource {
    
    // MARK: - Pages
    
    static func makeIntroPage() -> BLTNPageItem {
        let page = BLTNPageItem(title: "Welcome to \nequation")
        page.image = #imageLiteral(resourceName: "abacus")
        page.imageAccessibilityLabel = "Abacus"
        page.appearance.actionButtonColor = #colorLiteral(red: 0.2980392157, green: 0.8196078431, blue: 0.3803921569, alpha: 1)
        page.appearance.imageViewTintColor = #colorLiteral(red: 0.2980392157, green: 0.8196078431, blue: 0.3803921569, alpha: 1)
        page.appearance.actionButtonTitleColor = .white    
        
        page.descriptionText = "A very minimal calculator."
        page.actionButtonTitle = "Get started"
        
        page.dismissalHandler = { item in
            NotificationCenter.default.post(name: .SetupDidComplete, object: item)
        }
        
        page.actionHandler = { item in
            item.manager?.dismissBulletin(animated: true)
        }
        
        return page
    }
    
    // MARK: - User Defaults
    
    /// Whether user completed setup.
    static var userDidCompleteSetup: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "UserDidCompleteSetup")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "UserDidCompleteSetup")
        }
    }
    
}

// MARK: - Notifications

extension Notification.Name {
    
    /**
     * The setup did complete.
     *
     * The user info dictionary is empty.
     */
    
    static let SetupDidComplete = Notification.Name("SetupDidCompleteNotification")
    
}

//
//  EquationsViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/11.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import Firebase
import Presentr

class EquationsViewController: UIViewController {
    
    let presenter = Presentr(presentationType: .bottomHalf)
    
    let identifier = "EquationsCollectionViewCell"
    private let spacing: CGFloat = 10.0
    
    let stopwatch = Stopwatch()
    var isPaused = true
    
    var equationType: EquationType = .addition
    var answerList: [Int] = []
    var equationNumbers: [Int] = []
    var leftNumber = 0
    var rightNumber = 0
    var answer = 0
    
    var progress = 1
    var points = 0
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var equationView: UIView!
    @IBOutlet weak var operatorImage: UIImageView!
    @IBOutlet weak var leftNumberLabel: UILabel!
    @IBOutlet weak var rightNumberLabel: UILabel!
    
    @IBOutlet weak var pauseView: UIVisualEffectView!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    
    @IBOutlet weak var gameOverView: UIVisualEffectView!
    @IBOutlet weak var totalPointsLabel: UILabel!
    @IBOutlet weak var gameOverDoneButton: UIButton!
    
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressViewRightConstraint: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
        createEquation()
        presentHelpViewController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        progressViewRightConstraint.constant -= equationView.bounds.width
        
        // MARK: collectionView Spacing Setup
        collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.collectionView?.collectionViewLayout = layout
    }
    
    // MARK: Button Actions
    @IBAction func onPauseTap(_ sender: Any) {
        UIView.animate(withDuration: 0.50) {
            self.pauseView.isHidden = false
            self.pauseView.alpha = 1
        }
        stopwatch.stop()
    }
    
    @IBAction func onHelpTap(_ sender: Any) {
        UserDefaults.standard.set(answer, forKey: "answer")
        presentAnswerViewController()
    }
    
    @IBAction func onResumeTap(_ sender: Any) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25) {
                self.pauseView.alpha = 0
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.pauseView.isHidden = true
                }
            }
            self.startTimer()
        }
    }
    
    @IBAction func onRestartTap(_ sender: Any) {
        
    }
    
    @IBAction func onQuitTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onGameOverDoneTap(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            Api.Points.addHistory(amount: self.points, category: self.equationType.rawValue, time: self.timerLabel.text ?? "")
        })
    }
    
}

// MARK: UI Setup
extension EquationsViewController {
    func setupUI() {
        switch equationType {
        case .addition:
            progressView.layer.backgroundColor = #colorLiteral(red: 1, green: 0.5579856634, blue: 0.2126515806, alpha: 0.5)
            equationView.layer.backgroundColor = #colorLiteral(red: 1, green: 0.5579856634, blue: 0.2126515806, alpha: 1)
            operatorImage.image = UIImage(named: "pluss")
            break
        case .subtraction:
            progressView.layer.backgroundColor = #colorLiteral(red: 1, green: 0.3374335468, blue: 0.4082192779, alpha: 0.5)
            equationView.layer.backgroundColor = #colorLiteral(red: 0.9984052777, green: 0.3374335468, blue: 0.4082192779, alpha: 1)
            operatorImage.image = UIImage(named: "subtract")
            break
        case .multiplication:
            progressView.layer.backgroundColor = #colorLiteral(red: 0.222694695, green: 0.2233129442, blue: 0.3891766071, alpha: 0.5)
            equationView.layer.backgroundColor = #colorLiteral(red: 0.222694695, green: 0.2233129442, blue: 0.3891766071, alpha: 1)
            operatorImage.image = UIImage(named: "multiply")
            break
        case .division:
            progressView.layer.backgroundColor = #colorLiteral(red: 0.2535181046, green: 0.8357771039, blue: 0.8877439499, alpha: 0.5)
            equationView.layer.backgroundColor = #colorLiteral(red: 0.2535181046, green: 0.8357771039, blue: 0.8877439499, alpha: 1)
            operatorImage.image = UIImage(named: "dividee")
            break
        }
    }
}

// MARK: Display Intro
extension EquationsViewController {
    
    func startTimer() {
        Timer.scheduledTimer(timeInterval: 0.1, target: self,
            selector: #selector(EquationsViewController.updateElapsedTimeLabel(_:)), userInfo: nil, repeats: true)
        stopwatch.start()
    }
    
    @objc func updateElapsedTimeLabel(_ timer: Timer) {
        if stopwatch.isRunning {
            timerLabel.text = stopwatch.elapsedTimeAsString
        } else {
            timer.invalidate()
        }
    }
}

// MARK: Presentr setup
extension EquationsViewController {
    func presentHelpViewController() {
        let helpVC = storyboard!.instantiateViewController(withIdentifier: "HelpViewController")
        presenter.transitionType = nil
        presenter.blurBackground = true
        presenter.blurStyle = .dark
        presenter.roundCorners = true
        presenter.cornerRadius = 25
        presenter.dismissOnSwipe = false
        presenter.backgroundTap = .noAction
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: helpVC, animated: true)
    }
    
    func presentAnswerViewController() {
        let viewController = storyboard!.instantiateViewController(withIdentifier: "AnswerViewController")
        presenter.transitionType = nil
        presenter.blurBackground = true
        presenter.blurStyle = .dark
        presenter.roundCorners = true
        presenter.cornerRadius = 25
        presenter.dismissOnSwipe = true
        presenter.backgroundTap = .dismiss
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: viewController, animated: true)
    }
}

// MARK: Random Number Generator
extension EquationsViewController {
    func getRandomNumbers(maxNumber: Int, listSize: Int)-> [Int] {
        precondition(listSize < maxNumber, "Cannot generate a list of \(listSize) unique numbers, if they all have to be less than \(maxNumber)")

        var randomNumbers = (array: [Int](), set: Set<Int>())

        while randomNumbers.set.count < listSize {
            let randomNumber = Int(arc4random_uniform(UInt32(maxNumber+1)))
            if randomNumbers.set.insert(randomNumber).inserted { // If the number is unique
                randomNumbers.array.append(randomNumber) // then also add it to the arary
            }
        }

        return randomNumbers.array
    }
    
    func createEquation() {
        answerList = getRandomNumbers(maxNumber: 20, listSize: 8)
        equationNumbers = getRandomNumbers(maxNumber: 10, listSize: 2)
        leftNumberLabel.text = String(equationNumbers[0])
        rightNumberLabel.text = String(equationNumbers[1])
        determineAnswer()
    }
    
    func determineAnswer() {
        leftNumber = equationNumbers[0]
        rightNumber = equationNumbers[1]
        
        switch equationType {
        case .addition:
            answer = leftNumber + rightNumber
            break
        case .subtraction:
            if leftNumber < rightNumber {
                leftNumberLabel.text = String(equationNumbers[1])
                rightNumberLabel.text = String(equationNumbers[0])
                answer = rightNumber - leftNumber
            } else {
                answer = leftNumber - rightNumber
            }
            break
        case .multiplication:
            answer = leftNumber * rightNumber
            break
        case .division:
            answer = leftNumber / rightNumber
            break
        }
        print(answer)
        checkIfListHasAnswer()
    }
    
    func checkIfListHasAnswer() {
        if !answerList.contains(answer) {
            answerList.removeLast()
            answerList.append(answer)
            answerList.shuffle()
            collectionView.reloadData()
        } else {
            print("Has answer")
        }
    }
}

// MARK: CollectionView
extension EquationsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 10
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        if let collection = self.collectionView {
            let width = (collection.bounds.width - totalSpacing) / numberOfItemsPerRow
            return CGSize(width: width, height: (collection.bounds.height - 50) / 4)
        } else {
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return answerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? EquationsCollectionViewCell {
            
            cell.answerLabel.text = String(answerList[indexPath.row])
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedAnswer = answerList[indexPath.row]
        
        if selectedAnswer == answer {
            print("correct")
            points += 10
            progress += 1
            UIView.animate(withDuration: 0.25, animations: {
                self.view.layer.backgroundColor = #colorLiteral(red: 0.2916205823, green: 0.7956758142, blue: 0.5887959003, alpha: 1)
            }) { (_) in
                UIView.animate(withDuration: 0.25) {
                    self.view.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
            }
            proceedToNextQuestion()
        } else {
            print("incorrect")
            points -= 1
            progress += 1
            UIView.animate(withDuration: 0.25, animations: {
                self.view.layer.backgroundColor = #colorLiteral(red: 0.9984052777, green: 0.3374335468, blue: 0.4082192779, alpha: 1)
            }) { (_) in
                UIView.animate(withDuration: 0.25) {
                    self.view.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
            }
            proceedToNextQuestion()
        }
    }
}

// MARK: Next Question
extension EquationsViewController {
    func proceedToNextQuestion() {
        if progress <= 10 {
            pointsLabel.text = "\(String(points))"
            progressLabel.text = "\(String(progress)) | 10"
            let width = equationView.bounds.width
            progressViewRightConstraint.constant += width / CGFloat(progress)
            self.view.layoutIfNeeded()
            answerList = getRandomNumbers(maxNumber: 20, listSize: 8)
            createEquation()
            collectionView.reloadData()
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.gameOverView.isHidden = false
                self.gameOverView.alpha = 1
            }) { (_) in
                self.totalPointsLabel.text = "\(String(self.points))"
            }
            stopwatch.stop()
        }
    }
}

enum EquationType: String {
    case addition = "Addition"
    case subtraction = "Subtraction"
    case multiplication = "Multiplication"
    case division = "Division"
}

//
//  AnswerViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/12.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit

class AnswerViewController: UIViewController {

    @IBOutlet weak var answerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        answerLabel.text = String(UserDefaults.standard.integer(forKey: "answer"))
    }

}

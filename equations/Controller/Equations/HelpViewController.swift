//
//  HelpViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/13.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onLetsGoTap(_ sender: Any) {
        DispatchQueue.main.async {
            if let viewController = self.presentingViewController as? EquationsViewController {
                viewController.startTimer()
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

//
//  AddFriendViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import JGProgressHUD
import BarcodeScanner

class AddFriendViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var qrCodeImage: UIImageView!
    
    var hud = JGProgressHUD(style: .light)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupQrCode()
        
    }
    
    @IBAction func onBarcodeScanTap(_ sender: Any) {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        
        viewController.headerViewController.titleLabel.text = "Scan QR Code"

        present(viewController, animated: true, completion: nil)
    }
}

extension AddFriendViewController: BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        self.hud.textLabel.text = "Invitation Sent"
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            controller.dismiss(animated: true, completion: {
                let invite = Invitation(id: code)
                Api.Invitations.sendInvitation(invite: invite)
                self.hud.dismiss()
            })
        }
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
      controller.resetWithError(message: "An error occurred.\nPlease try again.")
    }
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
      controller.dismiss(animated: true, completion: nil)
    }
}

extension AddFriendViewController {
    func setupQrCode() {
        Api.User.retrieveUserInfo { (user) in
            self.qrCodeImage.image = self.generateQRCode(user.id)
        }
    }
    
    func generateQRCode(_ string: String) -> UIImage {
          
          if !string.isEmpty {
              
              let data = string.data(using: String.Encoding.ascii)
              
              let filter = CIFilter(name: "CIQRCodeGenerator")
              // Check the KVC for the selected code generator
            filter?.setValue(data, forKey: "inputMessage")
              
              let transform = CGAffineTransform(scaleX: 10, y: 10)
              let output = filter?.outputImage?.transformed(by: transform)
              
              return UIImage(ciImage: output!)
          } else {
              return UIImage()
          }
    }
}

//
//  LeaderboardViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import Presentr
import Firebase

class LeaderboardViewController: UIViewController {

    @IBOutlet weak var headerConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageView: UIImageView!
    
    @IBOutlet weak var invitesButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let identifier = "LeaderboardTableViewCell"
    var hasInvitation = false
    
    var friends: [User] = []
    
    let presenter = Presentr(presentationType: .bottomHalf)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkForInvitations()
        retrieveFriends()
    }
    
    func checkForInvitations() {
        Api.Invitations.retrieveInvitations { (invitation) in
            if invitation.id != "" {
               self.hasInvitation = true
               self.changeImage()
            } else {
                self.hasInvitation = false
                self.changeImage()
            }
       }
    }
    
    func changeImage() {
        switch self.hasInvitation {
        case true:
            self.invitesButton.setImage(UIImage(named: "new-invitation"), for: .normal)
            break
        case false:
            self.invitesButton.setImage(UIImage(named: "no-invitation"), for: .normal)
            break
        }
    }
    
    @IBAction func onAddFriendTap(_ sender: Any) {
        presentIntroViewController()
    }

}

extension LeaderboardViewController {
    func retrieveFriends() {
        self.friends.removeAll()
        Api.User.retrieveFriends { (user) in
            Api.User.retrieveUserInfo(for: user) { (user) in
                self.friends.append(user)
                self.friends.sort { (user, otherUser) -> Bool in
                    user.points > otherUser.points
                }
                self.tableView.reloadData()
            }
        }
    }
}

extension LeaderboardViewController {
    func presentIntroViewController() {
        let inviteVC = storyboard!.instantiateViewController(withIdentifier: "AddFriendViewController")
        presenter.transitionType = nil
        
        presenter.blurBackground = true
        presenter.blurStyle = .dark
        
        presenter.roundCorners = true
        presenter.cornerRadius = 25
        presenter.dismissOnSwipe = false
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: inviteVC, animated: true)
    }
}

extension LeaderboardViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? LeaderboardTableViewCell {
            
            let friend = friends[indexPath.row]
            
            cell.rankLabel.text = "\(indexPath.row + 1)"
            cell.nameLabel.text = "\(friend.givenName) \(friend.familyName)"
            cell.pointsLabel.text = "\(friend.points)"
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
}

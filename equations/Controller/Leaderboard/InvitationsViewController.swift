//
//  InvitationsViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import Firebase

class InvitationsViewController: UIViewController {

    @IBOutlet weak var noInvitationsView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let identifier = "InvitationsTableViewCell"
    let currentUser = UserDefaults.standard.string(forKey: "userId")
    var invitations: [Invitation] = []
    
    override func viewWillAppear(_ animated: Bool) {
        retrieveInvitations()
        checkInvitationsCount()
        
        tableView.isHidden = true
        noInvitationsView.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        tableView.tableFooterView = UIView()
    }
    
    func checkInvitationsCount() {
        Api.Invitations.retrieveInvitations { (invitation) in
            if self.invitations.count != 0 {
                print(invitation)
                if invitation.id != "" {
                    self.tableView.isHidden = false
                    self.noInvitationsView.isHidden = true
                } else {
                    self.tableView.isHidden = true
                    self.noInvitationsView.isHidden = false
                }
            }
        }
    }
    
    @IBAction func onDoneTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Retrieve Invitations
extension InvitationsViewController {
    func retrieveInvitations() {
        Api.Invitations.retrieveInvitations { (invitation) in
            if invitation.id != "" {
                self.invitations.removeAll()
                self.invitations.append(invitation)
                self.tableView.reloadData()
                self.checkInvitationsCount()
            } else {
                self.tableView.reloadData()
                self.checkInvitationsCount()
            }
        }
    }
}

// MARK: - tableView Delegate & DataSource
extension InvitationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        invitations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? InvitationsTableViewCell {
            cell.delegate = self
            
            let userId = invitations[indexPath.row].id
            
            Api.User.retrieveUserInfo(for: userId ?? "") { (user) in
                cell.nameLabel.text = "\(user.givenName) \(user.familyName)"
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
}

extension InvitationsViewController: InvitationsTableViewCellDelegate {
    func onAcceptTap(_ sender: UIButton) {
        if let indexPath = getCurrentCellIndexPath(sender) {
            Api.Invitations.acceptInvitation(invitation: self.invitations[indexPath.row]) {
                self.retrieveInvitations()
                self.tableView.reloadData()
            }
        }
    }
    
    func onDeclineTap(_ sender: UIButton) {
        if let indexPath = getCurrentCellIndexPath(sender) {
            if let user = invitations[indexPath.row].id {
                Api.Invitations.declineUser(userId: user) {
                    self.retrieveInvitations()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func getCurrentCellIndexPath(_ sender: UIButton) -> IndexPath? {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableView)
        if let indexPath: IndexPath = tableView.indexPathForRow(at: buttonPosition) {
            return indexPath
        }
        return nil
    }
}

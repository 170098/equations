//
//  IntroViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/09.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import AVFoundation
import AuthenticationServices

class IntroViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    
    override func viewWillAppear(_ animated: Bool) {
        setupAppleIDCredentialObserver()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupProviderLoginView()
    }
    
    func setupProviderLoginView() {
        let authorizationButton = ASAuthorizationAppleIDButton()
        authorizationButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
        self.stackView.addArrangedSubview(authorizationButton)
    }
    
    @objc
    func handleAuthorizationAppleIDButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
}

extension IntroViewController: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            UserDefaults.standard.set(userIdentifier, forKey: "userId")
            UserDefaults.standard.set(fullName?.givenName, forKey: "givenName")
            UserDefaults.standard.set(fullName?.familyName, forKey: "familyName")
            UserDefaults.standard.set(email, forKey: "email")
            
            do {
                try KeychainItem(service: "za.co.symptom.equations", account: "userIdentifier").saveItem(userIdentifier)
            } catch {
                print("Unable to save userIdentifier to keychain.")
            }
            
            Api.User.newUser(id: userIdentifier, givenName: fullName?.givenName ?? "", familyName: fullName?.familyName ?? "", email: email ?? "")
            UserDefaults.standard.set(true, forKey: "UserSignedIn")
            
            self.dismiss(animated: true) {
                if let viewController = self.presentingViewController as? DashboardViewController {
                    viewController.setupLabel(name: fullName?.givenName)
                }
            }
            
        }
    }
    
    private func setupAppleIDCredentialObserver() {
      let authorizationAppleIDProvider = ASAuthorizationAppleIDProvider()

      authorizationAppleIDProvider.getCredentialState(forUserID: KeychainItem.currentUserIdentifier) {
        (credentialState: ASAuthorizationAppleIDProvider.CredentialState, error: Error?) in
        
        if let error = error {
          print(error)
          // Something went wrong check error state
          return
        }
        
        switch (credentialState) {
        case .authorized:
            // The Apple ID credential is valid.
            UserDefaults.standard.set(true, forKey: "UserSignedIn")
            break
        case .revoked:
            // The Apple ID credential is revoked.
            UserDefaults.standard.set(false, forKey: "UserSignedIn")
            break
        case .notFound:
            // No credential was found, so show the sign-in UI.
            UserDefaults.standard.set(false, forKey: "UserSignedIn")
            break
        default:
            UserDefaults.standard.set(false, forKey: "UserSignedIn")
            break
        }
      }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
    }
}

extension IntroViewController: ASAuthorizationControllerPresentationContextProviding {
    //For present window
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

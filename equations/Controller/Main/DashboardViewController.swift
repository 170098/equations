//
//  DashboardViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/09.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import Firebase

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var schmecklesAmountLabel: UILabel!
    @IBOutlet weak var bestTimeLabel: UILabel!
    @IBOutlet weak var bestTimeCategoryLabel: UILabel!
    
    @IBOutlet weak var headerConstraint: NSLayoutConstraint!
    @IBOutlet weak var additionSubtractionConstraint: NSLayoutConstraint!
    @IBOutlet weak var divisionMultiplyConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var additionButton: DesignablePopBounceButton!
    @IBOutlet weak var subtractionButton: DesignablePopBounceButton!
    @IBOutlet weak var divisionButton: DesignablePopBounceButton!
    @IBOutlet weak var multiplicationButton: DesignablePopBounceButton!
    
    let equationsVC = EquationsViewController()
    var equationType: EquationType = .addition
    
    let currentUser = UserDefaults.standard.string(forKey: "userId")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let name = UserDefaults.standard.string(forKey: "givenName")
        setupLabel(name: name)
    }
    
    @IBAction func onCategoryTap(_ sender: Any) {
        let tag = (sender as? UIButton)?.tag
        
        switch tag {
        case 0:
            equationType = .addition
            presentViewController(for: .addition)
            break
        case 1:
            equationType = .subtraction
            presentViewController(for: .subtraction)
            break
        case 2:
            equationType = .division
            presentViewController(for: .division)
            break
        case 3:
            equationType = .multiplication
            presentViewController(for: .multiplication)
            break
        default:
            break
        }
    }
}

// MARK: - Present ViewController
extension DashboardViewController {
    func presentViewController(for equationType: EquationType) {
        performSegue(withIdentifier: "toEquations", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is EquationsViewController {
            let vc = segue.destination as? EquationsViewController
            switch equationType {
            case .addition:
                vc?.equationType = .addition
                break
            case .division:
                vc?.equationType = .division
                break
            case .multiplication:
                vc?.equationType = .multiplication
                break
            case .subtraction:
                vc?.equationType = .subtraction
                break
            }
        }
    }
}

// MARK: - Label Setup
extension DashboardViewController {
    func setupLabel(name: String?) {
        
        // MARK: Welcome Label
        guard let givenName = name else {
            return
        }
        self.welcomeLabel.text = "Welcome, \(givenName)"
        
        // MARK: Best Time Label
        Api.History.retrieveBestTime { (history) in
            self.bestTimeLabel.text = history.time
            self.bestTimeCategoryLabel.text = history.category
        }
        
        // MARK: Schmeckles Label
        Api.User.retrieveUserInfo { (user) in
            self.schmecklesAmountLabel.text = String(user.schmeckles)
        }
    }
}

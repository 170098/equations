//
//  ViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/09/19.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - IBOutlets & Variables
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentEquation: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var equalsButton: DesignablePopBounceButton!
    @IBOutlet weak var memoryReadClearButton: DesignablePopBounceButton!
    
    let lockOperations = [".", "x", "+", "-", "/"]
    var allowAppending = true
    var isOperationLocked = false
    
    var memory = "0"
    
    var operations = [Equation]() {
        didSet {
            operations.reverse()
            self.tableView.reloadData()
        }
    }
    
    var currentText = "0" {
        didSet {
            currentEquation.text = currentText
        
            if currentText.isEmpty {
                currentEquation.text = "0"
                currentText = "0"
            }
        }
    }
    
    // MARK: - Customization
    
    private var shouldHideStatusBar: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.tableFooterView = UIView()
        memory = UserDefaults.standard.string(forKey: "memory") ?? "0"
    }
    
    // MARK: - IBActions
    
    @IBAction func onThreeLongPress(_ sender: UILongPressGestureRecognizer) {
        showGameAlert()
    }
    
    @IBAction func onClearMemoryHold(_ sender: UILongPressGestureRecognizer) {
        memory = "0"
        UserDefaults.standard.set(memory, forKey: "memory")
    }
    
    @IBAction func onMemoryReadPressed(_ sender: UIButton) {
        currentText = memory
    }
    
    @IBAction func onMemoryAddPressed(_ sender: UIButton) {
        guard currentText != "0" else { return }
        
        let memoryValue = "\(memory)+\(currentText)"
        
        let expression = NSExpression(format: memoryValue)
        guard let mathValue = expression.expressionValue(with: nil, context: nil) as? Double else { return }
        
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        
        guard let value = formatter.string(from: NSNumber(value: mathValue)) else { return }
        
        memory = value
        currentText = value
        UserDefaults.standard.set(value, forKey: "memory")
        print(memory)
    }
    
    @IBAction func onMemoryMinusPressed(_ sender: UIButton) {
        guard currentText != "0" else { return }
        
        let memoryValue = "\(currentText)-\(memory)"
        
        let expression = NSExpression(format: memoryValue)
        guard let mathValue = expression.expressionValue(with: nil, context: nil) as? Double else { return }
        
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        
        guard let value = formatter.string(from: NSNumber(value: mathValue)) else { return }
        
        memory = value
        currentText = value
        UserDefaults.standard.set(value, forKey: "memory")
    }
    
    @IBAction func onLeftSwipe(_ sender: Any) {
        if currentText != "0" {
            currentText = "\(currentText.dropLast())"
        }
    }
    
    @IBAction func onClearPressed(_ sender: Any) {
        allowAppending = true
        if currentText == "0" { operations.removeAll() }
        currentText = "0"
    }
    
    @IBAction func onOperatorPressed(_ sender: Any) {
        allowAppending = true
        let button = (sender as! UIButton)
        
        if currentText != "0" {
            currentText = currentText.appending(String(button.titleLabel!.text!))
        }
        
        if lockOperations.contains((currentText.last?.description)!) && button.tag == -1 {
            equalsButton.isEnabled = false
            return
        } else if button.tag == -1 && currentText == "0" {
            return
        }
    }
    
    @IBAction func onNumberPressed(_ sender: Any) {
        
        let tag = (sender as! UIButton).tag
        equalsButton.isEnabled = true
        
        if currentText == "0" {
            currentText = String(tag)
        } else if !allowAppending {
            allowAppending = true
            currentText = String(tag)
        } else {
            currentText = currentText.appending(String(tag))
        }

    }
    
    @IBAction func onEqualsPressed(_ sender: Any) {
        allowAppending = false
        
        guard currentText != "0" else { return }
        
        let equation = Equation()
        equation.equation = currentText
        
        let expression = NSExpression(format: currentText)
        guard let mathValue = expression.expressionValue(with: nil, context: nil) as? Double else { return }
        
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        
        guard let value = formatter.string(from: NSNumber(value: mathValue)) else { return }
        
        currentText = value
        
        if !allowAppending {
            equation.answer = value
            operations.append(equation)
        } else {
            return
        }
    }
    
    func showGameAlert() {
        let alert = UIAlertController(title: "equations.", message: "Well done on finding this.\nWould you like to play a game?", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "No Thank You", style: .destructive, handler: { _ in
            //Cancel Action
            self.currentEquation.text = "0"
        }))
        alert.addAction(UIAlertAction(title: "Let's Play",
                                      style: .default,
                                      handler: {(_: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
                                        let viewController = storyboard.instantiateViewController(identifier: "MainViewController")
                                        viewController.modalPresentationStyle = .fullScreen
                                        self.present(viewController, animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - TableView

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return operations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "previousEquationTableViewCell", for: indexPath)
        
        let equation = operations[indexPath.row]
        
        cell.textLabel?.textColor = .white
        cell.detailTextLabel?.textColor = .white
        
        cell.textLabel?.text = equation.answer
        cell.detailTextLabel?.text = equation.equation
        
        return cell
    }
    
}

//
//  MainViewController.swift
//  equations
//
//  Created by Alex de Waal on 2019/09/30.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit
import Presentr
import AVFoundation

class MainViewController: UIViewController {
    
    @IBOutlet weak var dashboardView: UIView!
    @IBOutlet weak var dashboardTabIcon: UIImageView!
    
    @IBOutlet weak var leaderboardView: UIView!
    @IBOutlet weak var leaderboardTabIcon: UIImageView!
    
    let presenter = Presentr(presentationType: .bottomHalf)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !UserDefaults.standard.bool(forKey: "UserSignedIn") {
            presentIntroViewController()
        }
    }
    
    func presentIntroViewController(completion: (() -> Void)? = nil) {
        let introVC = storyboard!.instantiateViewController(withIdentifier: "IntroViewController")
        presenter.transitionType = nil
        presenter.blurBackground = true
        presenter.blurStyle = .dark
        presenter.roundCorners = true
        presenter.cornerRadius = 25
        presenter.dismissOnSwipe = false
        presenter.backgroundTap = .noAction
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: introVC, animated: true)
    }
    
    @IBAction func onDashboardTabTap(_ sender: Any) {
        dashboardView.isHidden = false
        leaderboardView.isHidden = true
        
        dashboardTabIcon.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        leaderboardTabIcon.tintColor = #colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)
    }
    
    @IBAction func onLeaderboardTabTap(_ sender: Any) {
        dashboardView.isHidden = true
        leaderboardView.isHidden = false
        
        dashboardTabIcon.tintColor = #colorLiteral(red: 0.7404708266, green: 0.7404883504, blue: 0.7404789329, alpha: 1)
        leaderboardTabIcon.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
}

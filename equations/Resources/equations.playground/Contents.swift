import Foundation

func getRandomNumbers(maxNumber: Int, listSize: Int)-> [Int] {
    precondition(listSize < maxNumber, "Cannot generate a list of \(listSize) unique numbers, if they all have to be less than \(maxNumber)")

    var randomNumbers = (array: [Int](), set: Set<Int>())

    while randomNumbers.set.count < listSize {
        let randomNumber = Int(arc4random_uniform(UInt32(maxNumber+1)))
        if randomNumbers.set.insert(randomNumber).inserted { // If the number is unique
            randomNumbers.array.append(randomNumber) // then also add it to the arary
        }
    }

    return randomNumbers.array
}

getRandomNumbers(maxNumber: 20, listSize: 8)

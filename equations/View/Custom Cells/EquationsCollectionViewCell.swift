//
//  EquationsCollectionViewCell.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/11.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit

class EquationsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var answerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

//
//  InvitationsTableViewCell.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import UIKit

protocol InvitationsTableViewCellDelegate: class {
    func onAcceptTap(_ sender: UIButton)
    func onDeclineTap(_ sender: UIButton)
}

class InvitationsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    var accepted = false
    
    weak var delegate: InvitationsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onDeclineTap(_ sender: UIButton) {
        delegate?.onDeclineTap(sender)
    }
    
    @IBAction func onAcceptTap(_ sender: UIButton) {
        delegate?.onAcceptTap(sender)
    }
}

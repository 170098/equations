//
//  Points.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation
import Firebase

class PointsApi {
    var db = Firestore.firestore()
    let currentUser = UserDefaults.standard.string(forKey: "userId")
    
    func addHistory(amount: Int, category: String, time: String) {
        
        var ref: CollectionReference? = nil
        ref = db.collection("users")
        
        if let user = currentUser {
            let docRef = ref?.document(user)
            docRef?.collection("history").addDocument(data: [
                    "category": category,
                    "time": time,
                    "amount": amount
                ], completion: { (err) in
                    if let err = err {
                        print("Error adding document: \(err)")
                    } else {
                        print("Document added with ID: \(docRef?.documentID ?? "")")
                    }
            })
        }
        
        incrementPoints(amount: amount)
        
    }
    
    func incrementPoints(amount: Int) {
        if let user = currentUser {
            let userRef = db.collection("users").document(user)
            userRef.updateData([
                "points": FieldValue.increment(Int64(amount))
            ])
            determineSchmeckles(amount: amount)
        }
    }
    
    func determineSchmeckles(amount: Int) {
        if let user = currentUser {
            let userRef = db.collection("users").document(user)
            let reducedAmount = amount / 10
            
            userRef.updateData([
                "schmeckles": FieldValue.increment(Int64(reducedAmount))
            ])
            
        }
    }
    
}

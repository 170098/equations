//
//  Api.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation
import Firebase

struct Api {
    static var User = UserApi()
    static var Points = PointsApi()
    static var History = HistoryApi()
    static var Invitations = InvitationApi()
}

//
//  User.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation
import Firebase

class User {
    
    var id: String
    var givenName: String
    var familyName: String
    var email: String
    var points: Int
    var schmeckles: Int
    
    init(id: String, givenName: String, familyName: String, email: String, points: Int, schmeckles: Int) {
        self.givenName = givenName
        self.familyName = familyName
        self.email = email
        self.id = id
        self.points = points
        self.schmeckles = schmeckles
    }
    
    static func transformUser(dict: [String: Any]) -> User? {
        guard let givenName = dict["givenName"] as? String,
            let familyName = dict["familyName"] as? String,
            let email = dict["email"] as? String,
            let points = dict["points"] as? Int,
            let id = dict["id"] as? String,
            let schmeckles = dict["schmeckles"] as? Int else {
                return nil
        }
        
        let user = User(id: id, givenName: givenName, familyName: familyName, email: email, points: points, schmeckles: schmeckles)
        
        if let givenName = dict["givenName"] as? String {
            user.givenName = givenName
        }
        
        if let familyName = dict["familyName"] as? String {
            user.familyName = familyName
        }
        
        if let email = dict["email"] as? String {
            user.email = email
        }
        
        if let id = dict["id"] as? String {
            user.id = id
        }
        
        if let points = dict["points"] as? Int {
            user.points = points
        }
        
        if let schmeckles = dict["schmeckles"] as? Int {
            user.schmeckles = schmeckles
        }
        
        return user
    }
    
}

class UserApi {
    let db = Firestore.firestore()
    let currentUser = UserDefaults.standard.string(forKey: "userId")
    
    func newUser(id: String, givenName: String, familyName: String, email: String) {
        var ref: CollectionReference? = nil
        ref = db.collection("users")
        let docRef = ref?.document(id)
        
        docRef?.setData([
            "id": docRef?.documentID ?? "",
            "givenName": givenName,
            "familyName": familyName,
            "email": email,
            "schmeckles": 0,
            "points": 0
        ], completion: { (err) in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(docRef?.documentID ?? "")")
            }
        })
    }
    
    func retrieveUserInfo(for user: String, onSuccess: @escaping(User) -> Void) {
        
        var userRef: CollectionReference? = nil
        userRef = db.collection("users")
        let docRef = userRef?.document(user)
        
        docRef?.addSnapshotListener({ (snapshot, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                if let data = snapshot?.data() {
                    if let userData = User.transformUser(dict: data) {
                        onSuccess(userData)
                    }
                }
            }
        })
    }
    
    func retrieveUserInfo(onSuccess: @escaping(User) -> Void) {
        guard let user = currentUser else {
            return
        }
        
        var userRef: CollectionReference? = nil
        userRef = db.collection("users")
        let docRef = userRef?.document(user)
        
        docRef?.addSnapshotListener({ (snapshot, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                if let data = snapshot?.data() {
                    if let userData = User.transformUser(dict: data) {
                        onSuccess(userData)
                    }
                }
            }
        })
    }
    
    func retrieveFriends(onSuccess: @escaping(String) -> Void) {
        guard let user = currentUser else {
            return
        }
        let userRef = db.collection("users").document(user)
        let friendsRef = userRef.collection("friends")
        
        friendsRef.addSnapshotListener { (snapshot, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
            } else {
                for document in snapshot!.documents {
                    let data = document.data()
                    onSuccess(data["id"] as! String)
                }
            }
        }
    }
}

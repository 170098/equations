//
//  OperatorType.swift
//  equations
//
//  Created by Alex de Waal on 2019/09/24.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation

enum OperatorType {
    case equal, clear, cancel, function
    
    var action: Int {
        switch self {
        case .equal: return -2
        case .clear: return -3
        case .cancel: return -4
        case .function: return -1
        }
    }
}

//
//  Invitations.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation
import Firebase

class Invitation {
    
    var id: String?
    var documentId: String?
    
    init(id: String) {
        self.id = id
    }
    
    static func parseData(dict: [String: Any]) -> Invitation? {
        guard let id = dict["id"] as? String,
        let documentId = dict["documentId"] as? String else {
                return nil
        }
        
        let invitation = Invitation(id: id)
        invitation.documentId = documentId
        
        return invitation
        
    }
}

class InvitationApi {
    
    let db = Firestore.firestore()
    let currentUser = UserDefaults.standard.string(forKey: "userId")
    
    func retrieveInvitations(onSuccess: @escaping(Invitation) -> Void) {
        guard let user = currentUser else {
            return
        }
        
        var userRef: CollectionReference? = nil
        userRef = db.collection("users")
        let docRef = userRef?.document(user)
        
        docRef?.collection("invitations").addSnapshotListener({ (snapshot, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                for document in snapshot!.documents {
                    if let invitation = Invitation.parseData(dict: document.data()) {
                        onSuccess(invitation)
                    }
                }
                
                guard let snapshot = snapshot else {
                    print("Error fetching snapshots: \(error!)")
                    return
                }
                
                let emptyDict = [
                    "id": ""
                ]
                
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .removed) {
                        if !snapshot.isEmpty {
                            if let invitation = Invitation.parseData(dict: diff.document.data()) {
                                onSuccess(invitation)
                            }
                        } else {
                            if let invitation = Invitation.parseData(dict: emptyDict) {
                                onSuccess(invitation)
                            }
                        }
                    }
                }
            }
        })
    }
    
    func sendInvitation(invite: Invitation) {
        Api.User.retrieveUserInfo(for: invite.id!) { (user) in
            var userRef: CollectionReference? = nil
            userRef = self.db.collection("users").document(user.id).collection("invitations")
            var ref: DocumentReference? = nil
            
            ref = userRef?.addDocument(data: [
                "documentId": ref?.documentID ?? "",
                "id": self.currentUser ?? ""
                ], completion: { (error) in
                    if error != nil {
                        print(error?.localizedDescription ?? "")
                    } else {
                        ref?.setData([
                            "documentId": ref?.documentID ?? ""
                            ], merge: true, completion: { (error) in
                                if error != nil {
                                    print(error?.localizedDescription ?? "")
                                } else {
                                    invite.documentId = ref?.documentID ?? ""
                                }
                        })
                    }
            })
        }
    }
    
    func acceptInvitation(invitation: Invitation, completion: (() -> Void)? = nil) {
        var userRef: CollectionReference? = nil
        userRef = self.db.collection("users")
        
        if let user = currentUser {
            let docRef = userRef?.document(user) // Current User's Document
            
            docRef?.collection("friends").addDocument(data: [ // In the user's friends collection, add...
                "id": invitation.id!
                ], completion: { (error) in
                    if error != nil {
                        print(error?.localizedDescription ?? "")
                    } else {
                        self.declineUser(userId: invitation.documentId!) {
                            completion!()
                        }
                    }
            })
            
        }
    }
    
    func declineUser(userId: String, completion: (() -> Void)? = nil) {
        var userRef: CollectionReference? = nil
        userRef = self.db.collection("users")
        
        if let user = currentUser {
            let docRef = userRef?.document(user) // Current User's Document
            docRef?.collection("invitations").document(userId).delete() { err in
                if let err = err {
                    print("Error removing document: \(err)")
                } else {
                    print("Document successfully removed!")
                    completion!()
                }
            }
        }
    }
    
}

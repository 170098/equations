//
//  Equation.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/16.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation

class Equation {
    var answer: String?
    var equation: String?
}

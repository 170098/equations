//
//  History.swift
//  equations
//
//  Created by Alex de Waal on 2019/10/14.
//  Copyright © 2019 symptom. All rights reserved.
//

import Foundation
import Firebase

class History {
    var amount: Int
    var category: String
    var time: String
    
    init(amount: Int, category: String, time: String) {
        self.amount = amount
        self.category = category
        self.time = time
    }
    
    static func parseData(dict: [String: Any]) -> History? {
        guard let amount = dict["amount"] as? Int,
            let category = dict["category"] as? String,
            let time = dict["time"] as? String else {
                return nil
        }
        
        let history = History(amount: amount, category: category, time: time)
        
        return history
        
    }
}

class HistoryApi {
    
    let db = Firestore.firestore()
    let currentUser = UserDefaults.standard.string(forKey: "userId")
    
    func retrieveBestTime(onSuccess: @escaping(History) -> Void) {
        
        guard let user = currentUser else {
            return
        }
        
        var userRef: CollectionReference? = nil
        userRef = db.collection("users")
        
        let historyRef = userRef?.document(user).collection("history")
        
        historyRef?
            .order(by: "time")
            .limit(to: 1)
            .addSnapshotListener({ (snapshot, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                
                for document in snapshot!.documents {
                    if let history = History.parseData(dict: document.data()) {
                        onSuccess(history)
                    }
                }
            }
        })
    }
    
    func addBestToLeaderboard(onSuccess: @escaping(User) -> Void) {
        guard let user = currentUser else {
            return
        }
        
        let userRef = db.collection("users")
        
        userRef.document(user).addSnapshotListener({ (snapshot, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
            } else {
                if let data = snapshot?.data() {
                    if let user = User.transformUser(dict: data) {
                        onSuccess(user)
                    }
                }
            }
        })
    }
    
}

# equations

This project makes use of Cocoapods for UI Libraries and for the Firebase Swift SDK to save data to the Firestore database. To start using this project, open terminal and navigate to the project's root directory. Once there, run the following command.

    pod install

Once that is complete you'll notice a new **.xcworkspace** file in the project folder, make sure to open that one.

That's it.